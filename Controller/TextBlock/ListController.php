<?php

namespace Nitra\TextBlockBundle\Controller\TextBlock;

use Admingenerated\NitraTextBlockBundle\BaseTextBlockController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->get('admingenerator.queryfilter.doctrine_odm');
        $queryFilter->setQuery($query);

        
        if (isset($filterObject['name']) && null !== $filterObject['name']) {
            $queryFilter->addStringFilter("name", $filterObject['name']);
        }
        
        if (isset($filterObject['location']) && null !== $filterObject['location']) {
            $queryFilter->addDefaultFilter('location.$id', $filterObject['location']->getId());
        }
        
        if (isset($filterObject['isActive']) && null !== $filterObject['isActive']) {
            $queryFilter->addBooleanFilter("isActive", $filterObject['isActive']);
        }
        
    }
}