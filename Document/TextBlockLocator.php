<?php

namespace Nitra\TextBlockBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document
 */
class TextBlockLocator
{
    use \Nitra\StoreBundle\Traits\LocaleDocument;

    /**
     * @ODM\Id(strategy="NONE")
     * @var string
     */
    private $id;

    /**
     * @ODM\String
     * @Gedmo\Translatable
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    private $name;

    /**
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    private $stores;

    public function __construct()
    {
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (String) $this->name;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add store
     *
     * @param Nitra\StoreBundle\Document\Store $store
     */
    public function addStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores[] = $store;
    }

    /**
     * Remove store
     *
     * @param Nitra\StoreBundle\Document\Store $store
     */
    public function removeStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores->removeElement($store);
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores()
    {
        return $this->stores;
    }
}