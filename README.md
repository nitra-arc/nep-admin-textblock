# TextBlockBundle

## Описание

данный бандл предназначен для:

* **создания и редактирования текстовых блоков**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-textblockbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\TextBlockBundle\NitraTextBlockBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
NitraTextBlockBundle:
    resource:    "@NitraTextBlockBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
textBlock:
    translateDomain: 'menu'
    route: Nitra_TextBlockBundle_TextBlock_list
```